#!/usr/bin/python

import random
import string
import time

import requests

started_at = time.perf_counter()
response = requests.post(
    url="http://127.0.0.1:8080/put",
    data=''.join(random.choice(string.ascii_letters) for _ in range(10))
)
finished_at = time.perf_counter()
print(
    "[{} s]: {} - {}".format(
        (finished_at - started_at),
        response.status_code,
        response.content.decode()
    )
)
