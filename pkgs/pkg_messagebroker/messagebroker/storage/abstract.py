from typing import Sequence


class Storage:
    def items(self) -> Sequence[str]:
        raise NotImplementedError()

    def put(self, msg: str) -> None:
        raise NotImplementedError()
