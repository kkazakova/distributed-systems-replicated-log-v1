import logging
import time

from logging import Logger
from queue import Queue
from threading import Lock
from threading import Semaphore
from threading import Thread
from typing import List
from typing import Sequence

import requests

from messagebroker.storage.abstract import Storage


class SecondaryStorageQueueTask:
    task_id: int
    msg: str
    lock: Semaphore

    def __init__(
        self,
        task_id: int,
        msg: str,
        replication_confirm_count: int
    ) -> None:
        self.task_id = task_id
        self.msg = msg
        self.lock = Semaphore(replication_confirm_count)

        for _ in range(0, replication_confirm_count):
            self.lock.acquire()


class SecondaryStorage:
    _queue: Queue[SecondaryStorageQueueTask]
    _thread: Thread
    _logger: Logger
    _url: str

    def __init__(
        self,
        url: str,
    ) -> None:
        name = "secondary-storage[{}]".format(
            url
        )

        self._queue = Queue()
        self._thread = Thread(
            target=self._process_queue,
            name=name
        )
        self._logger = logging.getLogger(
            name=name
        )
        self._url = url

    def start(self) -> None:
        self._thread.start()

    def put(self, task: SecondaryStorageQueueTask):
        self._queue.put(task)

    def _process_queue(self) -> None:
        while (True):
            task = self._queue.get()

            while (True):
                started_at = time.perf_counter()
                self._logger.info(
                    "Task started [{}]".format(
                        task.task_id
                    )
                )

                try:
                    response = requests.post(
                        url="{}/put".format(self._url),
                        data=task.msg
                    )

                    if (response.status_code == 200):
                        task.lock.release()
                        self._logger.info(
                            "Task completed [{}], [{} s]".format(
                                task.task_id,
                                (time.perf_counter() - started_at)
                            )
                        )
                        break
                except Exception:
                    self._logger.error(
                        "Task failed [{}], [{} s]".format(
                            task.task_id,
                            (time.perf_counter() - started_at)
                        )
                    )
                    time.sleep(1)


class MainStorageQueueTask:
    task_id: int
    msg: str
    lock: Lock

    def __init__(
        self,
        task_id: int,
        msg: str
    ) -> None:
        self.task_id = task_id
        self.msg = msg
        self.lock = Lock()

        self.lock.acquire()


class MainStorage(Storage):
    _task_id_sequence: int
    _queue: Queue[MainStorageQueueTask]
    _put_lock: Lock
    _thread: Thread
    _logger: Logger
    _items: List[str]
    _secondary_storages: Sequence[SecondaryStorage]
    _replication_confirm_count: int

    def __init__(
        self,
        secondary_servers: Sequence[str],
        replication_confirm_count: int
    ) -> None:
        self._task_id_sequence = -1
        self._queue = Queue()
        self._put_lock = Lock()
        self._thread = Thread(
            target=self._process_queue,
            name="main-storage"
        )
        self._logger = logging.getLogger("main-storage")
        self._items = []

        secondary_storages: List[SecondaryStorage] = []
        for url in secondary_servers:
            secondary_storages.append(
                SecondaryStorage(
                    url=url
                )
            )
        self._secondary_storages = secondary_storages

        self._replication_confirm_count = replication_confirm_count

    def items(self) -> Sequence[str]:
        return self._items

    def put(self, msg: str) -> None:
        with (self._put_lock):
            task = MainStorageQueueTask(
                task_id=self._next_msg_id(),
                msg=msg
            )
            self._queue.put(task)

        task.lock.acquire()

    def start(self) -> None:
        self._thread.start()
        for secondary_storage in self._secondary_storages:
            secondary_storage.start()

    def _next_msg_id(self) -> int:
        self._task_id_sequence += 1

        return self._task_id_sequence

    def _process_queue(self) -> None:
        while (True):
            main_task = self._queue.get()

            started_at = time.perf_counter()
            self._logger.info(
                "Task started [{}]".format(
                    main_task.task_id
                )
            )
            secondary_task = SecondaryStorageQueueTask(
                task_id=main_task.task_id,
                msg=main_task.msg,
                replication_confirm_count=self._replication_confirm_count
            )

            for secondary_storage in self._secondary_storages:
                secondary_storage.put(secondary_task)

            with (secondary_task.lock):
                self._items.append(main_task.msg)

            self._logger.info(
                "Task completed [{}], [{} s]".format(
                    main_task.task_id,
                    (time.perf_counter() - started_at)
                )
            )

            main_task.lock.release()
