from __future__ import annotations

import json
import logging
import time

from http.server import BaseHTTPRequestHandler
from logging import Logger
from socketserver import TCPServer
from typing import Tuple
from typing import cast

from messagebroker.storage.abstract import Storage


class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if (self.path == "/items"):
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()

            body = json.dumps(self.http_server().storage.items())
            self.wfile.write(
                body.encode()
            )

    def do_POST(self):
        if (self.path == "/put"):
            started_at = time.perf_counter()
            content_len = int(self.headers.get('Content-Length'))
            content = self.rfile.read(content_len)
            msg = content.decode()

            try:
                self.http_server().storage.put(msg)
                self.success()
                self.http_server().logger.info(
                    msg="Add msg [{}] completed [{}]".format(
                        msg,
                        (time.perf_counter() - started_at)
                    )
                )
            except Exception:
                self.http_server().logger.info(
                    msg="Put msg [{}] failed [{}]".format(
                        msg,
                        (time.perf_counter() - started_at)
                    )
                )
                self.error("Put msg [{}] failed".format(msg))

    def http_server(self) -> HttpServer:
        return cast(HttpServer, self.server)

    def error(self, msg: str) -> None:
        self.send_response(500)
        self.send_header("Content-type", "application/json")
        self.end_headers()

        body = json.dumps({
            "success": False,
            "error": msg
        })
        self.wfile.write(
            body.encode()
        )

    def success(self) -> None:
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

        body = json.dumps({
            "success": True
        })
        self.wfile.write(
            body.encode()
        )


class HttpServer(TCPServer):
    storage: Storage
    logger: Logger

    def __init__(
        self,
        storage: Storage,
        server_address: Tuple[str, int]
    ) -> None:
        super().__init__(
            server_address=server_address,
            RequestHandlerClass=RequestHandler
        )

        self.storage = storage
        self.logger = logging.getLogger("http-server")
