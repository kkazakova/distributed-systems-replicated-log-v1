from distutils.core import setup
from typing import List

from setuptools import find_packages


def read_requirements() -> List[str]:
    with (open("requirements.txt", "r") as f):
        return f.read().splitlines()


setup(
    name='message-broker-secondary',
    version='0.0.0',
    install_requires=read_requirements(),
    packages=find_packages(
        include=["secondary", "secondary.*"]
    ),
    scripts=[
        "bin/secondary-server"
    ]
)
