from __future__ import annotations

import json
import random
import time

from http.server import BaseHTTPRequestHandler
from logging import Logger
from socketserver import TCPServer
from typing import Tuple
from typing import cast

from secondary.store import Store


class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if (self.path == "/items"):
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()

            body = json.dumps(self.http_server().store.items())
            self.wfile.write(
                body.encode()
            )

    def do_POST(self):
        if (self.path == "/put"):
            content_len = int(self.headers.get('Content-Length'))
            content = self.rfile.read(content_len).decode()
            self.http_server().store.put(content)
            delay = random.randint(1, 5)
            time.sleep(delay)
            self.success()
            self.http_server().logger.info(
                msg="Replication completed [{} s]".format(delay)
            )

    def http_server(self) -> SecondaryServer:
        return cast(SecondaryServer, self.server)

    def success(self) -> None:
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

        body = json.dumps({
            "success": True
        })
        self.wfile.write(
            body.encode()
        )


class SecondaryServer(TCPServer):
    store: Store
    logger: Logger

    def __init__(
        self,
        logger: Logger,
        server_address: Tuple[str, int]
    ) -> None:
        super().__init__(
            server_address=server_address,
            RequestHandlerClass=RequestHandler
        )

        self.store = Store()
        self.logger = logger
