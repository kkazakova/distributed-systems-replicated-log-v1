from typing import List


class Store:
    _items: List[str]

    def __init__(self) -> None:
        self._items = []

    def put(self, msg: str) -> None:
        self._items.append(msg)

    def items(self) -> List[str]:
        return self._items